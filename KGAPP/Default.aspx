﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="KGAPP.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="NAssets/login_assets/assets/img/favicon.ico"/>
    <link rel="apple-touch-icon" sizes="76x76" href="login_assets/assets/img/apple-icon.png"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>KG|Login</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- Bootstrap core CSS     -->
    <link href="Assets/login_assets/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="Assets/login_assets/assets/css/paper-kit.css?v=2.1.0" rel="stylesheet"/>

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="login_assets/assets/css/demo.css" rel="stylesheet" />

    <!--     Fonts and icons     -->
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,300,700' rel='stylesheet' type='text/css'/>
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="Assets/login_assets/assets/css/nucleo-icons.css" rel="stylesheet"/>

    <style>
        #divLoginFailed{
			display:none;
		}
    </style>

</head>
<body>
    <form id="form1" runat="server">

        <div class="wrapper">
            <div class="page-header" style="background-image: url('Assets/login_assets/assets/img/login-image.jpg');">
                <div class="filter"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 ml-auto mr-auto">
                            
                            <div class="card card-register bg-dark">
                                <div style="border-bottom:1px solid #fff;"><b>KG|</b>Krace Gennedy Ltd.</div>
                                <h4 class="title text-primary">Staff Login</h4>
                                
                                    <label>Email</label>
                                    <input runat="server" id="txtEmail" type="email" class="form-control" placeholder="Email" required="" maxlength="20"/>

                                    <label>Password</label>
                                    <input runat="server" id="txtPassword" type="password" class="form-control" placeholder="Password" required="" maxlength="20"/>
                                    <button runat="server" class="btn btn-primary btn-block btn-round" id="btnLogin" onserverclick="btnLogin_Click">Login</button>
                                <br />
                                <div class="alert alert-danger" id="divLoginFailed" runat="server">
				                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
				                    Incorrect username or password!
			                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer register-footer">
                        <h6>&copy; <script>document.write(new Date().getFullYear())</script> Copyright Krace Gennedy</h6>
                        <small><i>dev by B.Ahmed</i></small>
                    </div>
                </div>
            </div>
        </div>

    </form>
</body>

    <!-- Core JS Files -->
    <script src="login_assets/assets/js/jquery-3.2.1.js" type="text/javascript"></script>
    <script src="login_assets/assets/js/jquery-ui-1.12.1.custom.min.js" type="text/javascript"></script>
    <script src="login_assets/assets/js/tether.min.js" type="text/javascript"></script>
    <script src="login_assets/assets/js/bootstrap.min.js" type="text/javascript"></script>

    <!--  Paper Kit Initialization snd functons -->
    <script src="login_assets/assets/js/paper-kit.js?v=2.0.1"></script>

</html>
