﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KGAPP
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] != null)
            {
                if (Session["role"].ToString() == "admin")
                    Response.Redirect("Dashboards/Admin/DashBoard.aspx");
                else
                    Response.Redirect("Dashboards/Employee/DashBoard.aspx");
            }
        }

        public void btnLogin_Click(object sender, EventArgs e)
        {
            Models.AuthenticateUser user = new Models.AuthenticateUser();
            bool res = false;
            res = user.Authenticate(txtEmail.Value.ToString(), txtPassword.Value.ToString());
            
            if (res)
            {
                //creating login session variables
                Session["user"] = txtEmail.Value.ToString();
                Session["userid"] = user.userId.ToString();
                Session["role"] = user.role;
                Session["fname"] = user.firstName;
                Session["lname"] = user.lastName;
               
                divLoginFailed.Style["display"] = "none";
        
                if (Session["role"].ToString() == "admin")
                    Response.Redirect("Dashboards/Admin/DashBoard.aspx");
                else
                {
                    Models.Courier c = new Models.Courier();
                    Session["city"] = c.getEmployee(user.userId).city;
                    Response.Redirect("Dashboards/Employee/DashBoard.aspx");
                }
                    

            }
            else
            {
                divLoginFailed.Style["display"] = "block";
            }
        }
    }
}