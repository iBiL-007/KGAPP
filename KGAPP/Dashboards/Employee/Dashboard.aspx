﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="KGAPP.Dashboards.Employee.Dashboard" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png"/>
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>KG|Employee Dashboard</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="/Assets/main_assets/assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="/Assets/main_assets/assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="/Assets/main_assets/assets/css/paper-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />


    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet"/>
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'/>
    <link href="/Assets/main_assets/assets/css/themify-icons.css" rel="stylesheet"/>

    <script type="text/javascript">
       
        //var city = $("#userCity").text();
        //var apiKey = "70dcf5a45431b20215c391c594f62cf4";

        //
        

    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div class="wrapper">
            <div class="sidebar" data-background-color="white" data-active-color="success">
            
            <span id="userCity" hidden="hidden" runat="server"></span>
            
            <!--
                Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
                Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
            -->

                <div class="sidebar-wrapper">
                    <div class="logo">
                        <a href="#" class="simple-text">
                            Krace Gennedy
                        </a>
                    </div>

                    <ul class="nav">
                        <li class="active">
                            <a href="#">
                                <i class="ti-panel"></i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="main-panel">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar bar1"></span>
                                <span class="icon-bar bar2"></span>
                                <span class="icon-bar bar3"></span>
                            </button>
                            <a class="navbar-brand" href="#">Dashboard</a>
                        </div>
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <a href="#">
                                        <p runat="server" id="currentUser">Logged in as</p>
                                    </a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="ti-bell"></i>
                                            <p class="notification" id="notifCount" runat="server">0</p>
                                            <p>Notifications</p>
                                            <b class="caret"></b>
                                    </a>
                                    <ul class="dropdown-menu" runat="server" id="notifSubjects">
                                        
                                    </ul>
                                </li>
                                <li>
                                    <a href="#" runat="server" onserverclick="lnkLogout_Click" causesvalidation="false">
                                        <i class="ti-power-off"></i>
                                        <p>Logout</p>
                                    </a>
                                </li>
                            </ul>

                        </div>
                    </div>
                </nav>


                <div class="content">
                    <div class="container-fluid">
                    
                        <div class="row">

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="header">
                                        <h4 class="title"><i class="fa fa-bell-o"></i> Notifications</h4>
                                        <p class="category">Today's schedule notifications</p>
                                    </div>
                                    <div class="content">
                                        <div id="chartHours" class="ct-chart">
                                            <asp:Table runat="server" CssClass="table table-striped" ID="tblNotifications">
                                                <asp:TableHeaderRow>
                                                    
                                                    <asp:TableHeaderCell>Subject</asp:TableHeaderCell>
                                                    <asp:TableHeaderCell>Message</asp:TableHeaderCell>
                                                    
                                                </asp:TableHeaderRow>
                                            </asp:Table>
                                        </div>
                                        <div class="footer">
                                            <div class="chart-legend">
                                                <i class="fa fa-circle text-info"></i> 
                                                <i class="fa fa-circle text-danger"></i> 
                                                <i class="fa fa-circle text-warning"></i>
                                            </div>
                                            <hr/>
                                            <div class="stats">
                                                <i class="ti-reload"></i> Updated just now
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                    </div>
                </div>


                <footer class="footer">
                    <div class="container-fluid">
                        <div class="copyright pull-right">
                            &copy; <script>document.write(new Date().getFullYear())</script> <a href="#">Krace Gennedy</a> <small>a creative tim theme</small>
                        </div>
                    </div>
                </footer>

            </div>
        </div>
        <ul id="weatherCon" hidden="hidden" runat="server"></ul>
    </form>
</body>
    <!--   Core JS Files   -->
    <script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
    <script src="/Assets/main_assets/assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="/Assets/main_assets/assets/js/bootstrap.min.js" type="text/javascript"></script>


    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
	<script src="/Assets/main_assets/assets/js/paper-dashboard.js"></script>

    <script type="text/javascript">
        $(function () {
            var city = $("#userCity").text();
            var apiKey = "70dcf5a45431b20215c391c594f62cf4";
            $.getJSON('https://api.openweathermap.org/data/2.5/weather?q=' + city + ',jm&units=metric&appid=' + apiKey, function (data) {
                console.log(data);
                $('<li>'+data.weather[0].description+'</li>').appendTo("#weatherCon");
                //$.each(data, function () {
                    //$("#weatherCon").html("text");//
                   
               // });
            });
        });
    </script>

    
</html>
