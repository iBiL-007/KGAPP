﻿using KGAPP.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KGAPP.Dashboards.Employee
{
    public partial class Dashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (Session["user"] != null)
            {
                userCity.InnerText = Session["city"].ToString();
                currentUser.InnerText = "Logged in as " + Session["role"].ToString() +" ["+Session["user"].ToString()+"]";
            }
            else
                Response.Redirect("~/Default.aspx");

            Courier c = new Courier();

            string marker = "";
            if (weatherCon.InnerText.ToString() != null)
            {
                if (weatherCon.InnerText.Contains("rain") || weatherCon.InnerText.Contains("rain"))
                    marker = "Rainy Day";
                else
                    marker = "Sunny Day";
            }

            List<MyNotification> mnl = c.getMyNotifications(Session["role"].ToString(),marker);
            notifCount.InnerText = mnl.Count.ToString();

           
            string subs = "<li> ";
            List<ListItem> mns = new List<ListItem>();
            foreach (MyNotification mni in mnl)
            {
                subs += mni.subject + " </li>";
            }
            notifSubjects.InnerHtml = subs;

            showNotifications(mnl);
        }

        public void lnkLogout_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("~/Default.aspx");
        }

        public void showNotifications(List<Models.MyNotification> nl)
        {

            DataTable tbl = new DataTable();

            tbl.Columns.Add("subject");
            tbl.Columns.Add("message");
            

            if (nl != null)
            {

                foreach (Models.MyNotification ei in nl)
                {
                    TableRow tr = new TableRow();

                    TableCell tcSubject = new TableCell();
                    TableCell tcMessage = new TableCell();
                    

                    tcSubject.Text = ei.subject.ToString();
                    tcMessage.Text = ei.message;
                   

                    tr.Cells.Add(tcSubject);
                    tr.Cells.Add(tcMessage);
                   

                    tbl.Rows.Add(tr);
                    tblNotifications.Rows.Add(tr);

                }
            }
        }
    }
}