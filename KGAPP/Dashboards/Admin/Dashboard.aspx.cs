﻿using KGAPP.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KGAPP.Dashboards.Admin
{
    public partial class Dashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] != null)
            {
                currentUser.InnerText = "Logged in as " + Session["role"].ToString() + " [" + Session["user"].ToString() + "]";
            }
            else
                Response.Redirect("~/Default.aspx");
            

            Courier c = new Courier();
            List<Role> rl = c.getRoles();

            if (!Page.IsPostBack)
            {
                List<ListItem> roles = new List<ListItem>();
                foreach (Role ri in rl)
                {
                    roles.Add(new ListItem(ri.role));
                }
                ddlTargetRoles.DataTextField = "Text";
                ddlTargetRoles.DataValueField = "Value";
                ddlTargetRoles.DataSource = roles;
                ddlTargetRoles.DataBind();

                ddlEmprole.DataTextField = "Text";
                ddlEmprole.DataValueField = "Value";
                ddlEmprole.DataSource = roles;
                ddlEmprole.DataBind();
            }

            showNotifications(c.getNotifications());
            showEmployees(c.getEmployees());

        }

        public void lnkLogout_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("~/Default.aspx");
        }

        public void btnPostNotification_Click(object sender, EventArgs e)
        {
            var notif = new Notification();

            notif.marker = ddlMarker.SelectedItem.Text;
            notif.target_role = ddlTargetRoles.SelectedItem.Text;
            notif.subject = txtSubject.Value.ToString();
            notif.message = txtMessage.Value.ToString();
            if (txtDate.Text.ToString() == "")
                txtDate.Text = DateTime.Now.ToShortDateString();
            else
                notif.expiry_date = DateTime.Parse(txtDate.Text.ToString());

            Courier c = new Courier();
            c.postNotification(notif);
            Response.Redirect("~/Dashboards/Admin/DashBoard.aspx");
        }

        public void showNotifications(List<Notification> nl)
        {
            
            DataTable tbl = new DataTable();

            tbl.Columns.Add("rownum");
            tbl.Columns.Add("marker");
            tbl.Columns.Add("target");
            tbl.Columns.Add("subject");
            tbl.Columns.Add("message");
            tbl.Columns.Add("pdate");
            tbl.Columns.Add("edate");
            tbl.Columns.Add("actions");

            if (nl != null)
            {
                int rcnt = 1;
                foreach (Notification ni in nl)
                {
                    TableRow tr = new TableRow();

                    TableCell tcRowNos = new TableCell();
                    TableCell tcMarker = new TableCell();
                    TableCell tcTarget = new TableCell();
                    TableCell tcSubject = new TableCell();
                    TableCell tcMessage = new TableCell();
                    TableCell tcPostDate = new TableCell();
                    TableCell tcExpDate = new TableCell();
                    TableCell tcBtn = new TableCell();

                    tcRowNos.Text = rcnt.ToString();
                    tcMarker.Text = ni.marker;
                    tcTarget.Text = ni.target_role;
                    tcSubject.Text = ni.subject;
                    tcMessage.Text = ni.message;
                    tcPostDate.Text = ni.time_stamp.ToString();
                    tcExpDate.Text = ni.expiry_date.ToShortDateString();

                   LinkButton  bn = new LinkButton();

                    
                    bn.ToolTip = ni.id.ToString();
                    bn.Click += deleteNotification_Click;
                    bn.CssClass = "btn btn-danger btn-fill";
                    bn.Text = "DEL";
                    bn.CausesValidation = false;
                    tcBtn.Controls.Add(bn);

                    tr.Cells.Add(tcRowNos);
                    tr.Cells.Add(tcMarker);
                    tr.Cells.Add(tcTarget);
                    tr.Cells.Add(tcSubject);
                    tr.Cells.Add(tcMessage);
                    tr.Cells.Add(tcPostDate);
                    tr.Cells.Add(tcExpDate);
                    tr.Cells.Add(tcBtn);

                    tbl.Rows.Add(tr);
                    tblNotifications.Rows.Add(tr);
                    rcnt += 1;
                }
            }
        }

        public void deleteNotification_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;

            Courier c = new Courier();

            c.deleteNotification(Convert.ToInt32(btn.ToolTip));

            Response.Redirect("~/Dashboards/Admin/DashBoard.aspx");
        }

        public void btnAddEmployee_Click(object sender, EventArgs e)
        {
            Courier c = new Courier();
            var emp = new Models.Employee();

            emp.first_name = txtFname.Value.ToString();
            emp.last_name = txtLname.Value.ToString();
            emp.middle_name = txtMname.Value.ToString();
            emp.email = txtEmail.Value.ToString();

            emp.dob = DateTime.Parse(txtDob.Text.ToString());
            emp.telephone_no = txtPhone.Text;
            emp.address = txtAddress.Value.ToString();
            emp.address_location = txtAddLoc.Value.ToString();
            emp.city = ddlCity.SelectedItem.Text;
            emp.country = txtCountry.Value.ToString();
            var rid = c.getRoles().Single(Role => Role.role == ddlEmprole.SelectedItem.Text);
            emp.roleId = rid.id;

           
            c.addEmployee(emp);
            Response.Redirect("~/Dashboards/Admin/DashBoard.aspx");
        }

        public void showEmployees(List<Models.Employee> el)
        {

            DataTable tbl = new DataTable();

            tbl.Columns.Add("eid");
            tbl.Columns.Add("fname");
            tbl.Columns.Add("lname");
            tbl.Columns.Add("role");
            tbl.Columns.Add("city");
            tbl.Columns.Add("phone");
            tbl.Columns.Add("email");
            tbl.Columns.Add("actions");

            if (el != null)
            {
                
                foreach (Models.Employee ei in el)
                {
                    TableRow tr = new TableRow();

                    TableCell tcEid = new TableCell();
                    TableCell tcFname = new TableCell();
                    TableCell tcLname = new TableCell();
                    TableCell tcRole = new TableCell();
                    TableCell tcCity = new TableCell();
                    TableCell tcPhone = new TableCell();
                    TableCell tcEmail = new TableCell();
                    TableCell tcBtn = new TableCell();

                    tcEid.Text = ei.id.ToString();
                    tcFname.Text = ei.first_name;
                    tcLname.Text = ei.last_name;
                    tcRole.Text = ei.rolename;
                    tcCity.Text = ei.city;
                    tcPhone.Text = ei.telephone_no;
                    tcEmail.Text = ei.email;

                    LinkButton bn = new LinkButton();
                    

                    bn.ToolTip = ei.id.ToString();
                    bn.Click += deleteEmployee_Click;
                    bn.CssClass = "btn btn-danger btn-fill";
                    bn.Text = "DEL";
                    bn.CausesValidation = false;
                    
                    tcBtn.Controls.Add(bn);

                    tr.Cells.Add(tcEid);
                    tr.Cells.Add(tcFname);
                    tr.Cells.Add(tcLname);
                    tr.Cells.Add(tcRole);
                    tr.Cells.Add(tcCity);
                    tr.Cells.Add(tcPhone);
                    tr.Cells.Add(tcEmail);
                    tr.Cells.Add(tcBtn);

                    tbl.Rows.Add(tr);
                    tblEmployees.Rows.Add(tr);
                    
                }
            }
        }

        public void deleteEmployee_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;

            Courier c = new Courier();

            c.deleteEmployee(Convert.ToInt32(btn.ToolTip));

            Response.Redirect("~/Dashboards/Admin/DashBoard.aspx");
        }
    }
}