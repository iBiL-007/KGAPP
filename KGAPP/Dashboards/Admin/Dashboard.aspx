﻿<%@ Page Language="C#" UnobtrusiveValidationMode="None" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="KGAPP.Dashboards.Admin.Dashboard" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    
    <meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png"/>
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>KG|Dashboard</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="/Assets/main_assets/assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="/Assets/main_assets/assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="/Assets/main_assets/assets/css/paper-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />


    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet"/>
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'/>
    <link href="/Assets/main_assets/assets/css/themify-icons.css" rel="stylesheet"/>

</head>
<body>
    <form id="form1" runat="server">
    <div class="wrapper">
        <div class="sidebar" data-background-color="black" data-active-color="danger">

        <!--
            Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
            Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
        -->

            <div class="sidebar-wrapper">
                <div class="logo">
                    <a href="#" class="simple-text">
                        Krace Gennedy
                    </a>
                </div>

                <ul class="nav">
                    <li class="active" id="dashboard">
                        <a href="#" id="dashboardLink">
                            <i class="ti-panel"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <li id="notifications">
                        <a href="#" id="notifLink">
                            <i class="ti-bell"></i>
                            <p>Notifications</p>
                        </a>
                    </li>
                    <li id="employees">
                        <a href="#" id="employeesLink">
                            <i class="fa fa-users"></i>
                            <p>Employees</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="main-panel">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar bar1"></span>
                            <span class="icon-bar bar2"></span>
                            <span class="icon-bar bar3"></span>
                        </button>
                        <a class="navbar-brand" href="#">
                            <span id="sectionTitle">Dashboard</span>
                        </a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="#">
                                    <p runat="server" id="currentUser">Logged in as</p>
                                </a>
                            </li>
                            <li>
                                <a href="#" runat="server" onserverclick="lnkLogout_Click" causesvalidation="false">
                                    <i class="ti-power-off"></i>
                                    <p>Logout</p>
                                </a>
                            </li>
                        </ul>

                    </div>
                </div>
            </nav>


            <div class="content">
                <div class="container-fluid">
                    <div id="dashboardWrapper" style="display: block;">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="card" style="height:auto;">
                                    <div class="header">
                                        <h4 class="title">
                                            <i class="fa fa-cloud"></i>
                                            Kingston Weather Forecast
                                        </h4>
                                        <p class="category">For next 5 days</p>
                                    </div>
                                    <div class="content table-responsive">
                                        <div class="" style="width:100%; font-size:smaller;">
                                            <table id="kgnForecast" class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th><b>Day</b></th>
                                                        <th><b>Weather</b></th>
                                                        <th><b>Low</b> (<small>celcius</small>)</th>
                                                        <th><b>High</b> (<small>celcius</small>)</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="kgnForecastTbl">

                                                </tbody>
                                            </table>
                                        </div><br />
                                        <div class="footer">
                                            <div class="chart-legend">
                                                <i class="fa fa-circle text-info"></i> 
                                                <i class="fa fa-circle text-danger"></i> 
                                                <i class="fa fa-circle text-warning"></i> 
                                            </div>
                                            <hr/>
                                            <div class="stats">
                                                <i class="ti-reload"></i> Updated just now
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="header">
                                        <h4 class="title">
                                            <i class="fa fa-cloud"></i>
                                            Montego Weather Forecast
                                        </h4>
                                        <p class="category">For next 5 days</p>
                                    </div>
                                    <div class="content table-responsive">
                                        <div class="">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th><b>Day</b></th>
                                                        <th><b>Weather</b></th>
                                                        <th><b>Low</b> (<small>celcius</small>)</th>
                                                        <th><b>High</b> (<small>celcius</small>)</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="mbyForecastTbl">

                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="footer">
                                            <div class="chart-legend">
                                                <i class="fa fa-circle text-info"></i> 
                                                <i class="fa fa-circle text-danger"></i> 
                                                <i class="fa fa-circle text-warning"></i> 
                                            </div>
                                            <hr/>
                                            <div class="stats">
                                                <i class="ti-reload"></i> Updated just now
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <!-- Notification View -->
                    <div id="notificationsWrapper" style="display: none;">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="header">
                                        <h4 class="title">Notifications</h4>
                                        <p class="category">Notifications Settings</p>
                                        <div class="content table-responsive" style=" font-size:smaller;">
                                            
                                            <asp:ScriptManager ID="scriptMan1" runat="server"></asp:ScriptManager>
                                            <ajaxToolkit:TabContainer ID="notifCont" runat="server" ActiveTabIndex="0">
                                                <ajaxToolkit:TabPanel ID="notifPanel" runat="server" HeaderText="All Notifications">
                                                    <ContentTemplate>
                                                        <asp:Table runat="server" CssClass="table table-striped" ID="tblNotifications">
                                                            <asp:TableHeaderRow>
                                                                <asp:TableHeaderCell>Nos.</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell>Marker</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell>Target</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell>Subject</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell>Message</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell>Post Date</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell>Expiry Date</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="text-center"><i class="fa fa-trash-o"></i></asp:TableHeaderCell>
                                                            </asp:TableHeaderRow>
                                                        </asp:Table>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                                <ajaxToolkit:TabPanel  ID="addNotifPanel" runat="server" HeaderText="Add Notification">
                                                    <ContentTemplate>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label>Marker</label>
                                                                    <asp:DropdownList class="form-control border-input" runat="server" id="ddlMarker">
                                                                        <asp:ListItem Value="rainy">Rainy Day</asp:ListItem>
                                                                        <asp:ListItem Value="sunny">Sunny Day</asp:ListItem>
                                                                    </asp:DropdownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label>Target Role</label>
                                                                    <asp:DropdownList ID="ddlTargetRoles" runat="server" class="form-control border-input">
                                                                    </asp:DropdownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label>Expiry Date</label>
                                                                    <asp:TextBox TextMode="Date" class="form-control border-input"  id="txtDate" runat="server" required=""></asp:TextBox>
                                                                    <div class="">
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                                                            runat="server"
                                                                            ControlToValidate="txtDate"
                                                                            Display="Dynamic"
                                                                            ErrorMessage="Expiry date is required!" CssClass="text-danger"> 
                                                                        </asp:RequiredFieldValidator>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-8">
                                                                <div class="form-group">
                                                                    <label>Subject</label>
                                                                    <input id="txtSubject" class="form-control border-input"  placeholder="Subject" maxlength="35" runat="server"/>
                                                                    <div class="">
                                                                        <asp:RequiredFieldValidator ID="rfvSubject"
                                                                            runat="server"
                                                                            ControlToValidate="txtSubject"
                                                                            Display="Dynamic"
                                                                            ErrorMessage="Subject is required!" CssClass="text-danger">
                                                                        </asp:RequiredFieldValidator>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label>Message</label>
                                                                    <textarea rows="5" id="txtMessage" runat="server" class="form-control border-input"  placeholder="Your message here." maxlength="255" ></textarea>
                                                                    <div>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                                            runat="server"
                                                                            ControlToValidate="txtMessage"
                                                                            Display="Dynamic"
                                                                            ErrorMessage="Message is required!" CssClass="text-danger">
                                                                        </asp:RequiredFieldValidator>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <input type="button" runat="server" id="btnPostNoti" onserverclick="btnPostNotification_Click" class="btn btn-info btn-fill btn-wd" value="Post" CausesValidation="false"/>
                                                        </div>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                            </ajaxToolkit:TabContainer>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="employeesWrapper" style="display: none;">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="header">
                                        <h4 class="title">Employees</h4>
                                        <p class="category">Employee Settings</p>
                                        <div class="content table-responsive" style=" font-size:smaller;">
                                            
                                            <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0">
                                                <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="All Employees">
                                                    <ContentTemplate>
                                                        <asp:Table runat="server" CssClass="table table-striped" ID="tblEmployees">
                                                            <asp:TableHeaderRow>
                                                                <asp:TableHeaderCell>ID#.</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell>Firstname</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell>Lastname</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell>Role</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell>City</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell>Phone</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell>Email</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass=" text-justify"><i class="fa fa-trash-o"></i></asp:TableHeaderCell>
                                                            </asp:TableHeaderRow>
                                                        </asp:Table>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                                <ajaxToolkit:TabPanel  ID="TabPanel2" runat="server" HeaderText="Add Employee">
                                                    <ContentTemplate>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label>Firstname</label>
                                                                    <input type="text" id="txtFname" runat="server" maxlength="15" placeholder="Firstname" class="form-control border-input" required=""/>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label>Lastname</label>
                                                                    <input type="text" id="txtLname" runat="server" maxlength="15" placeholder="Lastname" class="form-control border-input" required=""/>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label>Middle Name</label>
                                                                    <input type="text" id="txtMname" runat="server" maxlength="15" placeholder="Middle name" class="form-control border-input"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label>D.O.B</label>
                                                                    <asp:TextBox TextMode="Date" id="txtDob" runat="server" class="form-control border-input" required=""></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label>Email</label>
                                                                    <input type="email" id="txtEmail" runat="server" maxlength="50" placeholder="jdoe@krace.com" class="form-control border-input" required=""/>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label>Phone</label>
                                                                    <asp:TextBox TextMode="Phone" id="txtPhone" runat="server" minlength="7" maxlength="16" placeholder="876-123-4567" class="form-control border-input" required=""></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label>Address</label>
                                                                    <input type="text" id="txtAddress" runat="server" maxlength="50" placeholder="21 Jump Street, Kgn. 10" class="form-control border-input" required=""/>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label>Address Location</label>
                                                                    <input type="text" id="txtAddLoc" runat="server" maxlength="50" placeholder="Address Location" class="form-control border-input"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label>City</label>
                                                                    <asp:DropdownList id="ddlCity" runat="server" class="form-control border-input" required="">
                                                                        <asp:ListItem>Kingston</asp:ListItem>
                                                                        <asp:ListItem>Montego Bay</asp:ListItem>
                                                                    </asp:DropdownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label>Country</label>
                                                                    <input type="text" id="txtCountry" runat="server" placeholder="Jamaica" maxlength="20" class="form-control border-input"/>
                                                                </div>
                                                            </div>
                                                        </div><hr />
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label>Assign Role</label>
                                                                    <asp:DropdownList id="ddlEmprole" runat="server" class="form-control border-input" required="">
                                                                    </asp:DropdownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <asp:Button runat="server" id="btnAddEmployee" OnClick="btnAddEmployee_Click" class="btn btn-info btn-fill btn-wd" Text="Add" CausesValidation="False" />
                                                        </div>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                            </ajaxToolkit:TabContainer>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    
                    
                </div>
            </div>


            <footer class="footer">
                <div class="container-fluid">
                    <div class="copyright pull-right">
                        &copy; <script>document.write(new Date().getFullYear())</script> <a href="#">Krace Gennedy</a> <small> (a creative tim theme)</small>
                    </div>
                </div>
            </footer>

        </div>
    </div>
    </form>
</body>

    <!--   Core JS Files   -->
    <script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
    <script src="/Assets/main_assets/assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="/Assets/main_assets/assets/js/bootstrap.min.js" type="text/javascript"></script>


    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
	<script src="/Assets/main_assets/assets/js/paper-dashboard.js"></script>

    

    <script type="text/javascript">
        $("#dashboardLink").on("click",function(e){//When dasboard clicked
                e.preventDefault();
                $("#dashboard").removeClass('active');
                $("#notifications").removeClass('active');
                $("#employees").removeClass('active');
                $("#dashboard").addClass("active");

                $("#sectionTitle").text("Dashboard");
                $("#dashboardWrapper").show();
                $("#notificationsWrapper").hide();
                $("#employeesWrapper").hide();
        });
        $("#notifLink").on("click", function(e){//When notifications clicked
                e.preventDefault();
                $("#dashboard").removeClass('active');//.addClass("option-item2");
                $("#notifications").removeClass('active');
                $("#employees").removeClass('active');
                $("#notifications").addClass("active");

                $("#sectionTitle").text("Notifications");
                $("#dashboardWrapper").hide();
                $("#notificationsWrapper").show();
                $("#employeesWrapper").hide();
        });
        $("#employeesLink").on("click",function(e){//When employees clicked
                e.preventDefault();
                $("#dashboard").removeClass('active');//.addClass("option-item2");
                $("#notifications").removeClass('active');
                $("#employees").removeClass('active');
                $("#employees").addClass("active");

                $("#sectionTitle").text("Employees");
                $("#dashboardWrapper").hide();
                $("#notificationsWrapper").hide();
                $("#employeesWrapper").show();
        });

        var city1 = "kingston";
        var apiKey = "70dcf5a45431b20215c391c594f62cf4";

        //
        $(function () {
            $.getJSON('https://api.openweathermap.org/data/2.5/forecast?q=' + city2 + ',jm&units=metric&appid=' + apiKey, function (data) {
                console.log(data);
                var cnt = 1;
                $.each(data, function () {
                    var iconSrc = "http://openweathermap.org/img/w/" + data.list[cnt].weather[0].icon + '.png';
                    $('<tr><td>' + data.list[cnt].dt_txt + '</td><td>' + '<img src="' + iconSrc + '"/>' +
                        data.list[cnt].weather[0].description +
                        '</td><td>' + data.list[cnt].main.temp_min + '</td><td>' +
                        data.list[cnt].main.temp_max + '</td>').appendTo("#kgnForecastTbl");

                    cnt = cnt + 8;
                });
            });

            var city2 = "montego bay";
            $.getJSON('https://api.openweathermap.org/data/2.5/forecast?q=' + city2 + ',jm&units=metric&appid=' + apiKey, function (data) {
                console.log(data);
                var cnt = 1;
                $.each(data, function () {
                    var iconSrc = "http://openweathermap.org/img/w/" + data.list[cnt].weather[0].icon + '.png';
                    $('<tr><td>' + data.list[cnt].dt_txt + '</td><td>' + '<img src="' + iconSrc + '"/>' +
                        data.list[cnt].weather[0].description +
                        '</td><td>' + data.list[cnt].main.temp_min + '</td><td>' +
                        data.list[cnt].main.temp_max + '</td>').appendTo("#mbyForecastTbl");

                    cnt = cnt + 7;
                });
            });
        });

    </script>
</html>
