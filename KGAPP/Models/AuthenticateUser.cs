﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace KGAPP.Models
{
    public class AuthenticateUser
    {
        public int userId { get; set; }
        public string role { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        

        public bool Authenticate(string username, string password)
        {
            SqlConnection conn = null;
            SqlDataReader sdr = null;
            
            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connect"].ToString());
                conn.Open();
                SqlCommand cmd = new SqlCommand("sp_AuthenticateUser", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@username", username));
                cmd.Parameters.Add(new SqlParameter("@password", HashPassword(password)));
                
                sdr = cmd.ExecuteReader();
                if (sdr.HasRows)
                {
                    while (sdr.Read())
                    {
                        userId = Convert.ToInt32(sdr["id"]);
                        role = sdr["role"].ToString();
                        firstName = sdr["first_name"].ToString();
                        lastName = sdr["last_name"].ToString();
                       
                    }
                    return true;
                }
            }
            catch (Exception)
            {

                return false;
         
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
                if (sdr != null)
                {
                    sdr.Close();
                }
            }
            return false;
        }

        private string HashPassword(string password)
        {
            var crypt = new System.Security.Cryptography.SHA256Managed();
            var hash = new System.Text.StringBuilder();
            byte[] crypto = crypt.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return hash.ToString();
        }
    }
}