﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace KGAPP.Models
{
    public class Courier
    {
        public void postNotification(Notification n)
        {
            SqlConnection conn = null;
            
            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connect"].ToString());
                conn.Open();
                SqlCommand cmd = new SqlCommand("sp_postNotification", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                
                cmd.Parameters.AddWithValue("@marker", n.marker);
                cmd.Parameters.AddWithValue("@t_role", n.target_role);
                cmd.Parameters.AddWithValue("@subject", n.subject);
                cmd.Parameters.AddWithValue("@message", n.message);
                cmd.Parameters.AddWithValue("@expiry_date", n.expiry_date);
               

                int x = cmd.ExecuteNonQuery();

            }
            catch (Exception)
            {
                return;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }
        }

        public List<Notification> getNotifications()
        {
            var notifications = new List<Notification>();
            SqlConnection conn = null;
            SqlDataReader sdr = null;

            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connect"].ToString());
                conn.Open();
                SqlCommand cmd = new SqlCommand("sp_getNotifications", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                sdr = cmd.ExecuteReader();

                if (sdr.HasRows)
                {
                    while (sdr.Read())
                    {
                        var notif = new Notification();

                        notif.id = Convert.ToInt32(sdr["id"]);
                        notif.marker = sdr["marker"].ToString();
                        notif.target_role = sdr["target_role"].ToString();
                        notif.subject = sdr["subject"].ToString();
                        notif.message = sdr["message"].ToString();
                        notif.time_stamp = Convert.ToDateTime(sdr["time_stamp"]);
                        notif.expiry_date = Convert.ToDateTime(sdr["expiry_date"]);


                        notifications.Add(notif);
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
                if (sdr != null)
                {
                    sdr.Close();
                }
            }

            return notifications;
        }

        public void deleteNotification(int nid)
        {
            SqlConnection conn = null;

            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connect"].ToString());
                
                conn.Open();
                SqlCommand cmd = new SqlCommand("sp_deleteNotification", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                
                cmd.Parameters.AddWithValue("@id", nid);

                int x = cmd.ExecuteNonQuery();

            }
            catch (Exception)
            {
                return;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }

        }

        public List<Role> getRoles()
        {
            var roles = new List<Role>();
            SqlConnection conn = null;
            SqlDataReader sdr = null;

            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connect"].ToString());
                conn.Open();
                SqlCommand cmd = new SqlCommand("sp_getRoles", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                sdr = cmd.ExecuteReader();

                if (sdr.HasRows)
                {
                    while (sdr.Read())
                    {
                        var role = new Role();

                        role.id = Convert.ToInt32(sdr["id"]);
                        role.role = sdr["role"].ToString();
                        role.date = Convert.ToDateTime(sdr["date"]);
                        
                        roles.Add(role);
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
                if (sdr != null)
                {
                    sdr.Close();
                }
            }

            return roles;
        }

        public void addEmployee(Employee e)
        {
            SqlConnection conn = null;

            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connect"].ToString());
                conn.Open();
                SqlCommand cmd = new SqlCommand("sp_addEmployee", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@fname", e.first_name);
                cmd.Parameters.AddWithValue("@lname", e.last_name);
                cmd.Parameters.AddWithValue("@mname", e.middle_name);
                cmd.Parameters.AddWithValue("@email", e.email);
                cmd.Parameters.AddWithValue("@dob", e.dob);
                cmd.Parameters.AddWithValue("@address", e.address);
                cmd.Parameters.AddWithValue("@phone", e.telephone_no);
                cmd.Parameters.AddWithValue("@city", e.city);
                cmd.Parameters.AddWithValue("@address_loc", e.address_location);
                cmd.Parameters.AddWithValue("@country", e.country);
                cmd.Parameters.AddWithValue("@roleId", e.roleId);


                int x = cmd.ExecuteNonQuery();

            }
            catch (Exception)
            {
                return;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }
        }

        public void deleteEmployee(int eid)
        {
            SqlConnection conn = null;

            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connect"].ToString());

                conn.Open();
                SqlCommand cmd = new SqlCommand("sp_deleteEmployee", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@id", eid);

                int x = cmd.ExecuteNonQuery();

            }
            catch (Exception)
            {
                return;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }

        }

        public List<Employee> getEmployees()
        {
            var employees = new List<Employee>();
            SqlConnection conn = null;
            SqlDataReader sdr = null;

            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connect"].ToString());
                conn.Open();
                SqlCommand cmd = new SqlCommand("sp_getEmployees", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                sdr = cmd.ExecuteReader();

                if (sdr.HasRows)
                {
                    while (sdr.Read())
                    {
                        var emp = new Employee();

                        emp.id = Convert.ToInt32(sdr["id"]);
                        emp.first_name = sdr["first_name"].ToString();
                        emp.last_name = sdr["last_name"].ToString();
                        emp.middle_name = sdr["middle_name"].ToString();
                        emp.email = sdr["email"].ToString();
                        emp.dob = Convert.ToDateTime(sdr["dob"]);
                        emp.roleId = Convert.ToInt32(sdr["role_id"]);
                        emp.rolename = sdr["role"].ToString();
                        emp.address = sdr["address"].ToString();
                        emp.address_location = sdr["address_location"].ToString();
                        emp.city = sdr["city"].ToString();
                        emp.telephone_no = sdr["telephone_no"].ToString();
                        emp.country = sdr["country"].ToString();
                        
                        employees.Add(emp);
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
                if (sdr != null)
                {
                    sdr.Close();
                }
            }

            return employees;
        }

        public Employee getEmployee(int eid)
        {
            SqlConnection conn = null;
            SqlDataReader sdr = null;
            var emp = new Employee();

            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connect"].ToString());
                conn.Open();
                SqlCommand cmd = new SqlCommand("sp_getEmployee", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", eid);

                sdr = cmd.ExecuteReader();

                if (sdr.HasRows)
                {
                    while (sdr.Read())
                    {
      
                        emp.id = Convert.ToInt32(sdr["id"]);
                        emp.first_name = sdr["first_name"].ToString();
                        emp.last_name = sdr["last_name"].ToString();
                        emp.middle_name = sdr["middle_name"].ToString();
                        emp.email = sdr["email"].ToString();
                        emp.dob = Convert.ToDateTime(sdr["dob"]);
                        emp.roleId = Convert.ToInt32(sdr["role_id"]);
                        emp.rolename = sdr["role"].ToString();
                        emp.address = sdr["address"].ToString();
                        emp.address_location = sdr["address_location"].ToString();
                        emp.city = sdr["city"].ToString();
                        emp.telephone_no = sdr["telephone_no"].ToString();
                        emp.country = sdr["country"].ToString();
                        
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
                if (sdr != null)
                {
                    sdr.Close();
                }
            }

            return emp;
        }

        public List<MyNotification> getMyNotifications(string role,string marker)
        {
            var notifs = new List<MyNotification>();
            SqlConnection conn = null;
            SqlDataReader sdr = null;

            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connect"].ToString());
                conn.Open();
                SqlCommand cmd = new SqlCommand("sp_getMyNotifications", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@role", role);
                cmd.Parameters.AddWithValue("@marker", marker);

                sdr = cmd.ExecuteReader();

                if (sdr.HasRows)
                {
                    while (sdr.Read())
                    {
                        var notif = new MyNotification();

                        notif.id = Convert.ToInt32(sdr["id"]);
                        notif.subject = sdr["subject"].ToString();
                        notif.message = sdr["message"].ToString();
                        notif.time_stamp = Convert.ToDateTime(sdr["time_stamp"]);

                        notifs.Add(notif);
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
                if (sdr != null)
                {
                    sdr.Close();
                }
            }

            return notifs;
        }
    }
}