﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KGAPP.Models
{
    public class Entities
    {
    }

    public class Notification
    {
        public int id { get; set; }
        public string marker { get; set; }
        public string target_role { get; set; }
        public string subject { get; set; }
        public string message { get; set; }
        public DateTime expiry_date { get; set; }
        public DateTime time_stamp { get; set; }
    }

    public class Role
    {
        public int id { get; set; }
        public string role { get; set; }
        public DateTime date { get; set; }
    }

    public class Employee
    {
        public int id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string middle_name { get; set; }
        public DateTime dob { get; set; }
        public string email { get; set; }
        public string telephone_no { get; set; }
        public string address { get; set; }
        public string address_location { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public int roleId { get; set; }
        public string rolename { get; set; }
    }

    public class MyNotification
    {
        public int id { get; set; }
        public string subject { get; set; }
        public string message { get; set; }
        public DateTime time_stamp { get; set; }
    }
}